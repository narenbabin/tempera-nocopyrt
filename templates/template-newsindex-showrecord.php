<?php
/*
 * Template Name: Newsindex: Show Record
 *
 * @package Cryout Creations
 * @subpackage tempera
 * @since tempera 0.5
 */
session_start();

include '../private/newsIndex.inc';
include '../private/web_functions.inc';


if (!checkVars($sessionVars, 'session')) {
        goHome("/");
}

//check the get stuff
$checkGetVars = array(
  'id',
  'type'
);



if (!validateSession()) {
    goHome();
}
get_header();
?>

<section id="container" class="<?php echo tempera_get_layout_class(); ?>">
<div id="content" role="main"> <p>
<?php
get_template_part( 'templates/partials/form', 'search-newsindex' );
?>
</p>

<div class="entry-content">

<?php
$dbh = dbConnect($user, $pass, $host, $db);

$id = $_GET['record'];

if ($_GET['type'] == 'necrology') {
    showNecrologyRecord($dbh, $id);
}
elseif ($_GET['type'] == 'news') {
    showNewsRecord($dbh, $id);
}
else {
    $dbh = null;
}

$dbh = null;


function  showNecrologyRecord($dbh, $id) {
    $query = "select originalDate, lastName, firstName, source, reel, notes from necrology_new where id = :id";

    try {

        $sth = $dbh->prepare($query);
        $sth->bindParam(':id', $id);
        $sth->execute();

        while ($row = $sth->fetch(PDO::FETCH_BOTH)) {
            $br = "<br/>";
            echo "<p>";
            echo "<strong>Name: </strong>" . $row[1] . ", " . $row[2] . "$br";
            echo "<strong>Date:</strong> " . $row[0] . $br;
            echo "<strong>Source:</strong> " . $row[3] . ", Reel #" . $row[4] . $br;
            echo "<strong>Notes:</strong> " . $row[5];
            echo "</p>";
        }
    }
    catch (Exception $e) {
        $dbError = $e->getMessage();
        sendMessage($message=$dbError, "Database error - show.php - necrology record", $to='website@cpl.org');
        echo "<p>We are having technical difficulties.  Please try again later. An email has been sent to the website administrator</p>";
        exit;
    }
}



function showNewsRecord($dbh, $id) {
    $query = "select a.title, a.articleDate, so.source, a.page from articles a join lookupTable l on a.articleID = l.articleID join subjects s on s.subjectID = l.subjectID join sources so on so.sourceID = a.sourceID and  s.subjectID = :id";

    try {

        $sth = $dbh->prepare($query);
        $sth->bindParam(':id', $id);
        $sth->execute();

        while ($row = $sth->fetch(PDO::FETCH_BOTH)) {
            $br = "<br/>";
            echo "<p>";
            echo "<strong>Title: </strong>" . ucwords(strtolower($row[0])) . $br;
            echo "<strong>Date:</strong> " . $row[1] . $br;
            echo "<strong>Source:</strong> " . $row[2] . $br;
            echo "<strong>Notes:</strong> " . $row[3];
            echo "</p>";
        }
    }
    catch (Exception $e) {
        $dbError = $e->getMessage();
        sendMessage($message = $dbError, "Database error - show.php - news record", $to = 'website@cpl.org');
        echo "<p>We are having technical difficulties.  Please try again later.</p>";
        exit;
    }

}
?>
<hr>
<h2>How To Obtain More Details:</h2>
<p> To request a digital scan of this death notice or news article, email the Center for Local & Global History at <a href="mailto:clgh@cpl.org">clgh@cpl.org</a> and provide the citation information provided above
or <a href="https://cpl.org/contact/question-about-the-cleveland-news-index-and-necrology-file/">complete the News Index and Necrology File form</a> providing the citation information.  Scans of the newspaper articles, including death notices, will be sent as PDFs unless otherwise requested.  You can request up to three digital scans of newspaper items per day.
</p>
<p>
You can also access Cleveland newspaper microfilm at <a href="https://cpl.org/main-library/
">the Center for Local & Global History</a> during regular Library hours.
</p>
<p> If you have a
<a href="https://cpl.org/aboutthelibrary/usingthelibrary/library-cards/">
Cleveland Public Library Card</a>, you can use the
<a href="http://ezp2.cpl.org/login?url=http://infoweb.newsbank.com/resources/?p=EANX-NB">
Plain Dealer Historical database </a> to access full-text copies of the Plain Dealer from 1845 to 1991.
</p>

</div><!-- #content -->
</div><!-- #entry-content -->
<?php
tempera_get_sidebar(); ?>
</section><!-- #container -->

<?php get_footer(); ?>
