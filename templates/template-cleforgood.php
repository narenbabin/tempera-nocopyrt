<?php

/*
 * Template Name: Cle for good
 *
 * @package Cryout Creations
 * @subpackage tempera
 * @since tempera 0.5
 */

get_header(); ?>

<section class="cfg-featured-image"></section>
<section id="container" class="one-column">
			
<div id="content" role="main">

<?php
// cryout_before_content_hook();
	 // $temperas= tempera_get_theme_options();
	 // foreach ($temperas as $key => $value) { ${"$key"} = $value; }
?>

<div id="entry-content"> 

<?php
// print_r($temperas);
// tempera_set_featured_thumb()
?>
 



<div class="home-topics">
<h2>Cleveland for Good <span> You shared, we listened. Cleveland Public Library patrons, partners, and employees tell their stories. </span> </h2>
	<div class="bg-wrapper"> 
		<div class="l-contained">
			<div class="topic">
			<a href="<?php echo get_site_url(); ?>/category/cle-for-good/for-good-communities/">
			<div class="title">Communities</div> <img alt="Read about the physical improvements we're making to our branch buildings" src="<?php echo get_stylesheet_directory_uri(); ?>/images/cfg-Communities_350x250.jpg"></a></img> </div>
			<div class="topic">
			<a href="<?php echo get_site_url(); ?>/category/cle-for-good/for-good-history">
			<div class="title">History</div> <img alt="Hear how Cleveland Public Library has been a part of the community since 1868" src="<?php echo get_stylesheet_directory_uri(); ?>/images/cfg-History_350x250.jpg"></a> </img></div>
			<div class="topic">
			<a href="<?php echo get_site_url(); ?>/category/cle-for-good/for-good-people/">
			<div class="title">People</div> <img alt="Hear from our patrons, partners, and employees firsthand to learn how the Library is changing lives, building communities, and making a difference. " src="<?php echo get_stylesheet_directory_uri(); ?>/images/cfg-People_350x250.jpg"></a></img></div>
			</div>
		<?php // while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<div class="topic">
				<?php
				$link = get_field( 'link_url' );
				$icon = get_field( 'topic_icon' );
				?>
				<a href="<?php echo $link; ?>">
					<img class="icon" src="<?php echo $icon; ?>" alt="icon">
					<div class="title"><?php the_title(); ?></div>
					<?php the_post_thumbnail( 'home-topic' ); ?>
				</a>
			</div>
		<?php // endwhile; ?>
		</div>
	</div>
</div>

<?php
// loading posts from the CLEforgood section
// get_template_part('content/content', 'cleforgood');
?>

</div>
</div>
</section>
<?php get_footer(); ?>
