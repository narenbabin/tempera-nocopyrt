<?php
/**
 * The SEARCH FORM
 *
 * @package Cryout Creations
 * @subpackage Tempera
 * @since Tempera 1.1
 */
// create unique integer to create labels for
$search_unique_id = esc_attr( wp_unique_id() );
?>

<form role="search" method="GET" class="form-search" 
action="<?php echo esc_url( home_url( '/newsindex/results' . '' ) ); ?>">
<label for="search-input-<?php echo ( $search_unique_id ); ?>" class="screen-reader-text">Enter what you want to search for:</label>
<input type="search" name="searchFor" id="search-input-<?php echo ( $search_unique_id ); ?>" required class="search-field" /> 

  <fieldset>
	<legend>Select a source for your search:</legend>
	<div class="radio-row">
	  <label for="both-option"><input type="radio" name="searchType" id="both-option" value="both" checked/>
	  Necrology File and News Index </label>
	  <label for="necrology-option"><input type="radio" name="searchType" id="necrology-option" value="necrology"/>
	  Necrology File only</label>
	  <label for="newsindex-option"><input type="radio" name="searchType" id="newsindex-option" value="newsindex"/>
	  News Index only</label>
	</div>
</fieldset>
<input class="cpl-button cpl-button-action--search" value="search" name="search" aria-label="Submit your search query" alt="Search" type="submit" />
</form>
