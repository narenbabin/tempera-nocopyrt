<?php

// partial template for the FMP subsite

?>
<div class="l-nav-container l-nav-container--reimagining">
			<div class="l-contained">
				<div class="l-nav">
					<a class="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i><i class="fa fa-times" aria-hidden="true"></i></a>
					<nav role="navigation">
						<ul class="menu">
									<li class="menu-item"><a href="<?php echo esc_url( get_site_url() ); ?>"><span>the</span> library</a>
								<?php
								// the main menu

									wp_nav_menu(
										array(
											'menu'       => 'reimagining_primary_menu',
											'menu_class' => 'menu-primary--reimagining',
											'theme_location' => 'reimagining_primary_menu_location',
										)
									);
									?>
									</li>
									<li class="menu-item active--reimagining"><a href="https://courbanize.com/collections/cpl"><span>Our Future</span>Is Building</a></li>
						</ul>
					</nav>
				</div> <!-- l-nav -->

			</div>  <!-- l-contained -->

			<?php get_template_part( 'templates/partials/header', 'search-button' ); ?>
		</div>  <!-- l-nav-container -->

