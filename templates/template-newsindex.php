<?php /*
 * Template Name: Newsindex
 *
 * @package Cryout Creations
 * @subpackage tempera
 * @since tempera 0.5
 */

session_start();
include '../private/newsIndex.inc';
include '../private/web_functions.inc';

get_header();

if (!isset($_SESSION['start']) || $_SESSION['start'] === 0) {
    initSession();
}


if (!validateSession()) {
    initSession();
}

?>

<section id="container" class="<?php echo tempera_get_layout_class(); ?>">
<div id="content" role="main">
<h1 class="entry-title">The Cleveland Necrology File and News Index</h1>

<?php
get_template_part( 'templates/partials/form', 'search-newsindex' );
?>

<div class="entry-content">
    <p>
        <strong>The Cleveland Necrology File</strong> was produced from a microfilmed copy of an alphabetical card file containing local cemetery records and newspaper death notices gathered by the staff of the Cleveland Public Library. It contains death notices published in the following newspapers:
        <ul>
        <li>The Cleveland Plain Dealer (1850-1975)</li>
        <li>The Cleveland Herald (1833, 1847-1848, 1876, 1878-1879)</li>
        <li>The Cleveland Press (1941-1975) </li>
        </ul>
    </p>

    <p><strong>This file is not an official or complete record</strong> of deaths in the Cleveland area during those years. It was not always customary during this time period for individuals to use paid newspaper notices. In addition, the cemetery records included in this database may not be complete. </p>
    <hr/>

    <p><strong>The Cleveland News Index</strong> lists citation information for local news stories, feature articles and reviews from: </p>
    <ul>
        <li>The Plain Dealer (1983-June 1999)</li>
        <li>Cleveland Magazine (1983-2014)</li>
        <li>Northern Ohio Live (Sept. 1990-June 2009)</li>
        <li>Ohio Magazine (Oct. 1990-2014)</li>
    </ul>
    <p>The Cleveland News Index lists citation information for death notices and obituaries from:</p>
    <ul>
    <li>The Cleveland Plain Dealer (1976-2014)</li>
    <li>The Cleveland Press (1976-1982)</li>
    </ul>

    <p> <strong>The Cleveland News Index does not provide full text access to these sources.</strong> </p>
    <hr/>
    <h2>Additional information:</h2>

    <p>
    For full text of current obituaries, visit <a href="http://obits.cleveland.com/obituaries/cleveland/">http://obits.cleveland.com/obituaries/cleveland/</a>.
    </p>
    <p>
    If you have a <a href="https://cpl.org/aboutthelibrary/usingthelibrary/library-cards/">
Cleveland Public Library Card</a>, you can use the <a href="http://ezp2.cpl.org/login?url=http://infoweb.newsbank.com/resources/?p=EANX-NB">Plain Dealer Historical database</a> to access full-text copies of the Plain Dealer from 1845 to 1991. </p>
    <p> A listing of available databases can be found at <a href="http://cpl.org/research-learning/researchdatabases/"> CPL's Research Databases</a>
    </p>

    <p>
    If you are unable to visit the Center for Local &amp; Global History, you can request up to three digital scans of newspaper items per day by <a href="https://cpl.org/contact/question-about-the-cleveland-news-index-and-necrology-file/">completing an obituary request form</a>.
    </p>

    <p> For requests, please provide the following information, if you have it: </p>
    <ul>
    <li>name of the deceased</li>
    <li>source and date of publication of the death notice</li>
    <li>and/or date of death</li>
    </ul>

</div><!-- #content -->
</div><!-- #entry-content -->

<?php tempera_get_sidebar(); ?>
</section><!-- #container -->

<?php get_footer(); ?>



