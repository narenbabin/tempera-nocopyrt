<?php
/**
 * Template Name: One column; Full Width
 *
 * A custom page template without any sidebar and full-width
 * there's no margin on left side;
 * it goes beyond the 1150px
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package Cryout Creations
 * @subpackage tempera
 * @since tempera 0.5
 */

get_header(); ?>

		<section id="container" class="one-column">

			<div id="content" role="main">
				<?php get_template_part( 'content/content', 'page' ); ?>

			</div><!-- #content -->

		</section><!-- #container -->

<?php get_footer(); ?>
