<?php
/**
 * Template Name: Libcal Queries
 * Template Post Type: post, page, libcalquery
 * For the post type "libcalquery"
 *
 * @package Cryout Creations
 * @subpackage tempera
 * @since tempera 0.5
 */
get_header();

?>
		<section id="container" class="<?php echo tempera_get_layout_class(); ?>">

			<div id="content" role="main">

				<?php get_template_part( 'content/content', 'libcalquery' ); ?>

			<?php cryout_after_content_hook(); ?>
			</div><!-- #content -->
			<?php tempera_get_sidebar(); ?>
		</section><!-- #container -->


<?php
get_footer();
?>
