<?php
/**
 * Template Name: One column, no sidebar
 *
 * for this template, Content is not full screen-width at larger viewpoints;
 * max-width for this template defined by the ($totalwidth) in parent theme
 *
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package Cryout Creations
 * @subpackage tempera
 * @since tempera 0.5
 */

get_header(); ?>

		<section id="container" class="one-column">

			<div id="content" role="main">

				<?php get_template_part( 'content/content', 'page'); ?>

			</div><!-- #content -->

		</section><!-- #container -->

<?php get_footer(); ?>
