<?php
/*
 * Template Name: All posts Archive
 *
 * @package Cryout Creations
 * @subpackage tempera
 * @since tempera 0.5
 */

get_header(); ?>

		<section id="container" class="<?php echo tempera_get_layout_class(); ?>">
			<div id="content" role="main">
			<?php
			cryout_before_content_hook();
			?>

<?php



	 $temperas = tempera_get_theme_options();
foreach ( $temperas as $key => $value ) {
	${"$key"} = $value; }

if ( get_query_var( 'paged' ) ) {
	$paged = get_query_var( 'paged' );
} elseif ( get_query_var( 'page' ) ) { // 'page' is used instead of 'paged' on Static Front Page
	$paged = get_query_var( 'page' );
} else {
	$paged = 1;
}


		$my_query_args = array(
			'post_type'      => array( 'post', 'cpl_agenda' ),
			'posts_per_page' => 6,
			'cat'            => '-76,-77,-78,-79',
			'post_status'    => 'publish',
			'orderby'        => 'date',
			'paged'          => $paged,
			'order'          => 'DESC',
		   // 'offset' => 5
		);
		$my_query = new WP_Query( $my_query_args );

		if ( $my_query->have_posts() ) :
			?>

				<header class="page-header">
				</header>

				<?php /* Start the Loop */ ?>
				<?php
				while ( $my_query->have_posts() ) :
					$my_query->the_post();
					?>

					<?php
						/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content/content', get_post_type( $post ) );


					?>

				<?php endwhile; ?>

			<?php

			allposts_pagination();

			?>

				<?php
				wp_reset_postdata();
				tempera_content_nav( 'nav-below' );
				?>

			<?php else : ?>

				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'tempera' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'tempera' ); ?></p>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>

			<?php cryout_after_content_hook(); ?>
			</div><!-- #content -->
		<?php tempera_get_sidebar(); ?>
		</section><!-- #primary -->


<?php get_footer(); ?>
