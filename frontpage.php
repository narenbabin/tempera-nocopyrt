<?php
/**
 * Frontpage generation functions
 * Creates the slider, the columns, the titles and the extra text
 *
 * @package tempera
 * @subpackage Functions
 */

//wp_enqueue_style( 'tempera-frontpage' );


	 $temperas = tempera_get_theme_options();
foreach ( $temperas as $key => $value ) {
	${"$key"} = $value; } ?>

<h1 class="screen-reader-text">Cleveland Public Library: The People's University</h1>

<?php
$args = array(
	'post_type'      => 'home_slide',
	'posts_per_page' => '5',
);
?>
<?php $loop = new WP_Query( $args ); ?>

<div class="home-slider">
<?php
while ( $loop->have_posts() ) :
	$loop->the_post();
	?>
	<div class="slick-image-slide slick-slide">
		<?php
		$link = get_field( 'link_url' );
		?>
		<?php the_post_thumbnail( 'full' ); ?>
		<div class="content">
			<div class="share">
				<span class="share-label">SHARE:</span> <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>" class="share-link"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/fb.png" alt="Share to Facebook"></a>	<a target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo $link; ?>" class="share-link"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/twitter.png" alt="Share to Twitter"></a>	<a href="mailto:?&body=Check out this site: <?php echo $link; ?>" class="share-link"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/email.png" alt="Send link in Email"></a>
			</div>
			<?php the_content(); ?>
			<div style="float:left;"><a href="<?php echo $link; ?>" class="cpl-button cpl-button-action">Learn More</a></div>
		</div>
	</div>
<?php endwhile; ?>
</div>

<script type="text/javascript">
	jQuery(document).ready(function($){
		$('.home-slider').slick({
			dots: false,
			infinite: true,
			arrows: true,
			speed: 350,
			autoplay: true,
			fade: true,
			autoplaySpeed: 10000,
			pauseOnHover: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			adaptiveHeight: false
		});
	});

</script>

<div class="home-topics">
<?php
$args = array(
	'post_type'      => 'home_topics',
	'post_status'    => 'publish',
	'posts_per_page' => '-1',
);
?>
<?php $loop = new WP_Query( $args ); ?>
	<div class="bg-wrapper">
		<div class="l-contained">
		<?php
		while ( $loop->have_posts() ) :
			$loop->the_post();
			?>
			<div class="topic">
				<?php
				$link = get_field( 'link_url' );
				$icon = get_field( 'topic_icon' );
				?>
				<a href="<?php echo $link; ?>">
					<img class="icon" src="<?php echo $icon; ?>" alt="icon">
					<div class="title"><?php the_title(); ?></div>
					<?php the_post_thumbnail( 'home-topic' ); ?>
				</a>
			</div>
		<?php endwhile; ?>
		</div>
	</div>

</div> <!-- home-topics -->

<?php
// Second FrontPage title
if ( $tempera_frontposts == 'Enable' ) :
	get_template_part( 'content/content', 'frontpage' );
endif;
?>

<div id="frontpage-view-link"><a class="cpl-button cpl-button-action" href="all-posts">Read all News stories</a></div>
