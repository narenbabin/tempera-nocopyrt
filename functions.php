<?php

/********************************************
*  custom "powered by" text to be displayed instead of
*  the copyright link; supports HTML; write your
*  text/HTML between the quotes; escape single
*  quotes in the text with backslash, eg:
*  $custom_footer_text = 'this is my site\'s "<i>custom powered by text</i>"';
*/
$custom_powered_by = '';

/********************************************
*  show/hide "Powered by WordPress" link
*  consult WordPress terms of use before changing this
*  values:
*     false = display link to WordPress
*     true  = hide link                    */
$hide_wordpress_link = true;


/****** no configurable options below ******/

function enqueue_scripts() {

	wp_dequeue_style( 'tempera-fonts' );
	wp_enqueue_style( 'normalize_css', 'https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css' );
	wp_register_script( 'misc-js', get_stylesheet_directory_uri() . '/js/scripts.js', '', '', true );
	wp_enqueue_script( 'misc-js' );
	wp_register_script( 'accessible-modal-window', get_stylesheet_directory_uri() . '/js/aria.modal.min.js', '', '', true );
	wp_enqueue_script( 'accessible-modal-window' );
	// accessible_modal_window requirement
	wp_register_script( 'inert-polyfill', 'https://cdn.jsdelivr.net/gh/GoogleChrome/inert-polyfill@v0.1.0/inert-polyfill.min.js', '', '', true );
	wp_enqueue_script( 'inert-polyfill' );

	if ( is_page( 'locations' ) ) {
		wp_register_style( 'mapbox_css', 'https://api.mapbox.com/mapbox.js/v3.3.1/mapbox.css' );

		wp_register_script( 'mapbox-js', 'https://api.mapbox.com/mapbox.js/v3.3.1/mapbox.js', '', '', true );

		wp_register_script( 'mapbox-custom', get_stylesheet_directory_uri() . '/js/my-mapbox-code.js', array( 'mapbox-js' ), '', true );

		wp_enqueue_style( 'mapbox_css' );
		wp_enqueue_script( 'mapbox-js' );
		wp_enqueue_script( 'mapbox-custom' );
	}
}
add_action( 'wp_enqueue_scripts', 'enqueue_scripts', 4 );

// load shortcodes
require get_stylesheet_directory() . '/inc/shortcodes.php';
require get_stylesheet_directory() . '/inc/search.php';
require get_stylesheet_directory() . '/inc/block-patterns.php';


// functions for libcal ; move this into mu-plugin soon!
function enqueue_libcal_scripts() {
	// if all libcal ACF values are null, don't enqueue any libcal scripts:
	if ( function_exists ( 'get_field' )){
		if ( get_field( 'libcal_location_field' ) || get_field( 'libcal_category_number' ) ) {
			wp_enqueue_script( 'moment-js', 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js', 'jquery', time(), true );
			// Load location specific information
			if ( is_page_template( 'templates/template-location.php' ) ) {
				wp_enqueue_script( 'libcalFetcher', get_stylesheet_directory_uri() . '/js/libcal-location.js', array( 'jquery', 'moment-js' ), time(), true );
				wp_localize_script(
					'libcalFetcher',
					'libcalACFData',
					array(
						'libcalLocationObject' => get_field( 'libcal_location_field' ),
						'libcalCategoryObject' => get_field( 'libcal_category_number' ),
					)
				);
			}
			if ( is_page_template( 'templates/template-music-at-main.php' ) ) {
					wp_enqueue_script( 'libcalFetcher', get_stylesheet_directory_uri() . '/js/libcal-music-at-main.js', array( 'jquery', 'moment-js' ), time(), true );
			} else {
				wp_enqueue_script( 'libcalFetcher', get_stylesheet_directory_uri() . '/js/libcal.js', array( 'jquery', 'moment-js' ), time(), true );
			}

			// regardless of which ACF template, call these 3 ACF fields, use them as JS objects
			wp_localize_script(
				'libcalFetcher',
				'libcalACFData',
				array(
					'libcalLocationObject' => get_field( 'libcal_location_field' ),
					'libcalCategoryObject' => get_field( 'libcal_category_number' ),
					'libcalDateObject'     => get_field( 'libcal_date_field' ),
				)
			);
		}
	}
}
add_action( 'wp_enqueue_scripts', 'enqueue_libcal_scripts', 5 );


function enqueue_2020_gallery_scripts() {
	// do not load this, if for some reason, ACF is not functioning
	if ( function_exists ( 'get_field' )) {
		if ( get_field( 'exhibit2020_wall_number' ) ) {
			// if there's a wall number, enqueue the JS for the gallery
			wp_enqueue_script( 'fetch-js', 'https://cdnjs.cloudflare.com/ajax/libs/fetch/2.0.4/fetch.min.js', '', time(), true );
			wp_enqueue_script( 'exhibit2020-js', get_stylesheet_directory_uri() . '/js/2020-exhibit.js', array( 'jquery' ), time(), true );
			//wp_enqueue_script( 'exhibit2020-source', get_stylesheet_directory_uri() . '/js/2020-exhibit-source.json', '', time(), true );
			wp_localize_script(
				'exhibit2020-js',
				'exhibit2020Object',
				array(
					'exhibit2020WallNumber' => get_field( 'exhibit2020_wall_number' ),
					'themeDirectory'        => get_stylesheet_directory_uri(),
				)
			);
		}
	}
}

add_action( 'wp_enqueue_scripts', 'enqueue_2020_gallery_scripts', 6 );

function skip_link_focus_fix() {
	// minified js/skip-link-focus-fix.js`.
	?>
	<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
	</script>
	<?php
}
add_action( 'wp_print_footer_scripts', 'skip_link_focus_fix' );

// enqueue parent theme styling
function child_parent_styling() {
	wp_enqueue_style( 'tempera-parent', get_template_directory_uri() . '/style.css' ); // main style.css
}
add_action( 'wp_head', 'child_parent_styling', 4 );

// remove tempera functions action hooks
function remove_tempera_functions() {
	remove_action( 'cryout_footer_hook', 'tempera_site_info', 99 );
}
add_action( 'init', 'remove_tempera_functions' );

function search_redirect_form() {
	if ( is_search() && ! empty( $_GET['search-form-source'] ) ) {
		if ( isset( $_GET['search-form-source'] ) && $_GET['search-form-source'] === 'catalog' ) {
			$query = urlencode( $_GET['s'] );
			wp_redirect( 'https://search.clevnet.org/client/en_US/cpl-main/search/results?qu=' . $query );
			exit();
		}
		if ( isset( $_GET['search-form-source'] ) && $_GET['search-form-source'] === 'digitalgallery' ) {
			$query = rawurlencode( $_GET['s'] );
			wp_redirect( 'http://cplorg.contentdm.oclc.org/cdm/search/searchterm/' . $query . '/field/all/mode/all/conn/and/order/nosort' );
			exit();
		}
		if ( isset( $_GET['search-form-source'] ) && $_GET['search-form-source'] === 'overdrive' ) {
			$query = urlencode( $_GET['s'] );
			wp_redirect( 'https://clevnet.overdrive.com/clevnet-cpl/content/search?query=' . $query );
			exit();
		} else {
			$query = urlencode( $_GET['s'] );
			wp_safe_redirect( home_url() . '?s=' . $query );
			exit();
		}
	}
}

add_action( 'pre_get_posts', 'search_redirect_form', 1 );

// child theme functions
function child_site_info() {
	global $hide_wordpress_link;
	global $custom_powered_by;
	?>
	<div style="display:block;float:right;text-align:right;padding:5px 20px 5px;text-transform:uppercase;font-size:11px;">
	<?php
	if ( $hide_wordpress_link == false ) :
		_e( 'Powered by', 'tempera' );
		?>
		 <a href="<?php echo esc_url( 'http://wordpress.org/' ); ?>"
			title="<?php esc_attr_e( 'Semantic Personal Publishing Platform', 'tempera' ); ?>"> <?php printf( ' %s', 'WordPress' ); ?></a>.
	<?php endif; ?>
	<?php if ( strlen( $custom_powered_by ) > 0 ) : ?>
		<?php echo do_shortcode( $custom_powered_by ); ?>
	<?php endif; ?>
	</div><!-- #site-info -->

	<?php
}

// attach child theme functions hooks
add_action( 'cryout_footer_hook', 'child_site_info', 99 );

add_image_size( 'home-topic', 390, 275, array( 'center', 'center' ) ); // Hard crop left top

// make child theme inherit all of Tempera's external WP options (header/bg/menus/widgets)
if ( get_stylesheet() !== get_template() ) {
	add_filter(
		'pre_update_option_theme_mods_' . get_stylesheet(),
		function ( $value, $old_value ) {
			update_option( 'theme_mods_' . get_template(), $value );
			return $old_value; // prevent update to child theme mods
		},
		10,
		2
	);
	add_filter(
		'pre_option_theme_mods_' . get_stylesheet(),
		function ( $default ) {
			return get_option( 'theme_mods_' . get_template(), $default );
		}
	);
};

add_filter( 'gform_submit_button', 'my_form_submit_button', 10, 2 );
function my_form_submit_button( $button, $form ) {
	$dom = new DOMDocument();
	$dom->loadHTML( $button );
	$input    = $dom->getElementsByTagName( 'input' )->item( 0 );
	$classes  = $input->getAttribute( 'class' );
	$classes .= ' cpl-button cpl-button-action';
	$input->setAttribute( 'class', $classes );
	return $dom->saveHtml( $input );
}

function tempera_set_featured_thumb() {
	if ( ! is_front_page() ) {
		// let it read temperas settings

		global $post;
		global $temperas;

		// if featured image as posthumbnail is enabled & there's a featured image - use it!
		$image_src = cryout_echo_first_image( $post->ID );
		if ( function_exists( 'has_post_thumbnail' ) && has_post_thumbnail() && ( $temperas['tempera_fpost'] == 'Enable' ) ) {
			the_post_thumbnail( 'custom', array( 'class' => 'align' . strtolower( $temperas['tempera_falign'] ) . ' post_thumbnail' ) );
		}

		// if no featured image, use first image in post
		elseif ( ( $temperas['tempera_fpost'] == 'Enable' ) && ( $temperas['tempera_fauto'] == 'Enable' ) && $image_src ) {
			echo '<a title="' . the_title_attribute( 'echo=0' ) . '" href="' . esc_url( get_permalink() ) . '" >
						<img width="' . $temperas['tempera_fwidth'] . '" title="" alt="" class="align' . strtolower( $temperas['tempera_falign'] ) . ' post_thumbnail" src="' . $image_src . '">
				  </a>';
		}
	}
};

// all posts pagination; will need to rewrite as a single function with the parent function
function allposts_pagination( $pages = '', $range = 2, $prefix = '' ) {
	 $showitems = ( $range * 2 ) + 1;

	 global $paged;
	if ( empty( $paged ) ) {
		$paged = 1;
	}

	if ( $pages == '' ) {
		global $my_query;
		$pages = $my_query->max_num_pages;
		if ( ! $pages ) {
			$pages = 1;
		}
	}

	if ( 1 != $pages ) {
		echo "<div class='pagination_container'><nav class='pagination'>";
		if ( $prefix ) {
			echo "<span id='paginationPrefix'>$prefix </span>";}
		if ( $paged > 2 && $paged > $range + 1 && $showitems < $pages ) {
			echo "<a href='" . get_pagenum_link( 1 ) . "'>&laquo;</a>";
		}
		if ( $paged > 1 && $showitems < $pages ) {
			echo "<a href='" . get_pagenum_link( $paged - 1 ) . "'>&lsaquo;</a>";
		}

		for ( $i = 1; $i <= $pages; $i++ ) {
			if ( 1 != $pages && ( ! ( $i >= $paged + $range + 1 || $i <= $paged - $range - 1 ) || $pages <= $showitems ) ) {
				echo ( $paged == $i ) ? "<span class='current'>" . $i . '</span>' : "<a href='" . get_pagenum_link( $i ) . "' class='inactive' >" . $i . '</a>';
			}
		}

		if ( $paged < $pages && $showitems < $pages ) {
			echo "<a href='" . get_pagenum_link( $paged + 1 ) . "'>&rsaquo;</a>";
		}
		if ( $paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages ) {
			echo "<a href='" . get_pagenum_link( $pages ) . "'>&raquo;</a>";
		}
		echo "</nav></div>\n";
	}
} // tempera_pagination()

add_filter( 'gform_include_thousands_sep_pre_format_number', '__return_false' );


// for bylines of posts, by: X, etc on this date
function tempera_meta_before() {
	global $temperas;

	// If single page take appropiate settings
	if ( is_single() ) {
		$temperas['tempera_blog_show'] = $temperas['tempera_single_show'];
	}
	// Post Author
	$output = '';
	if ( $temperas['tempera_blog_show']['author'] ) {
			// use generic name for all authors except laura who requested attribution
		$my_array = array( '16', '2', '24', '26', );
		if ( in_array( get_the_author_meta( 'ID' ), $my_array ) ) {
				$output .= sprintf(
					'<span class="author vcard" ><i class="crycon-author crycon-metas" title="' . __( 'Author ', 'tempera' ) . '"></i>
							<a class="url fn n" rel="author" href="%1$s" title="%2$s">CPL Staff writer</a></span>',
					esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
					sprintf( esc_attr( __( 'View all posts by The Staff', 'tempera' ) ), get_the_author() ),
					get_the_author()
				);
		} else {
				$output .= sprintf(
					'<span class="author vcard" ><i class="crycon-author crycon-metas" title="' . __( 'Author ', 'tempera' ) . '"></i>
					<a class="url fn n" rel="author" href="%1$s" title="%2$s">%3$s</a></span>',
					esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
					sprintf( esc_attr( __( 'View all posts by %s', 'tempera' ) ), get_the_author() ),
					get_the_author()
				);
		}
	}

	// Post date/time
	if ( $temperas['tempera_blog_show']['date'] || $temperas['tempera_blog_show']['time'] ) {
		$separator = '';
		$date      = '';
		$time      = '';

		if ( $temperas['tempera_blog_show']['date'] && $temperas['tempera_blog_show']['time'] ) {
			$separator = ' - ';
		}
		if ( $temperas['tempera_blog_show']['date'] ) {
			$date = get_the_date();
		}
		if ( $temperas['tempera_blog_show']['time'] ) {
			$time = esc_attr( get_the_time() );
		}

		$output .= '<span><i class="crycon-time crycon-metas" title="' . __( 'Date', 'tempera' ) . '"></i>
				<time class="onDate date published" datetime="' . get_the_time( 'c' ) . '">
					<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $date . $separator . $time . '</a>
				</time>
			   </span><time class="updated"  datetime="' . get_the_modified_date( 'c' ) . '">' . get_the_modified_date() . '</time>';
	}
	echo $output;

}; // tempera_meta_before()

// post categories at the end
function tempera_posted_in() {
	global $temperas;

	$tcategory_list = '';
	if ( $temperas['tempera_blog_show']['category'] && get_the_category_list() ) {
			$tcategory_list .= '<span class="bl_categ"><i class="crycon-folder-open crycon-metas" title="' . __( 'Categories', 'tempera' ) . '"></i>' .
			get_the_category_list( ', ' ) . '</span> ';
	}
	echo $tcategory_list;

}; // tempera_posted_in()

// overriding the breadcrumbs
function tempera_breadcrumbs() {

	$temperas = tempera_get_theme_options();

	$showOnHome  = 0;                                    // 1 - show breadcrumbs on the homepage, 0 - don't show
	$separator   = '<i class="crycon-angle-right"></i>';  // separator between crumbs
	$home        = '<a href="' . esc_url( home_url() ) . '"><i class="crycon-homebread"></i><span class="screen-reader-text">' . __( 'Home', 'tempera' ) . '</span></a>'; // text for the 'Home' link
	$showCurrent = 1;                                   // 1 - show current post/page title in breadcrumbs, 0 - don't show
	$before      = '<span class="current">';                 // tag before the current crumb
	$after       = '</span>';                                 // tag after the current crumb

	global $post;
	$homeLink = esc_url( home_url() );
	if ( is_front_page() && ( $temperas['tempera_frontpage'] == 'Enable' ) ) {
		return; }
	if ( is_home() && ( $temperas['tempera_frontpage'] != 'Enable' ) ) {

		if ( $showOnHome == 1 ) {
			echo '<div class="breadcrumbs"><a href="' . $homeLink . '"><i class="crycon-homebread"></i>' . __( 'Home Page', 'tempera' ) . '</a></div>';
		}
	} else {

		echo '<div class="breadcrumbs">' . $home . $separator . ' ';

		if ( is_category() ) {
			// category
			$thisCat = get_category( get_query_var( 'cat' ), false );
			if ( $thisCat->parent != 0 ) {
				echo get_category_parents( $thisCat->parent, true, ' ' . $separator . ' ' );
			}
			echo $before . ' "' . single_cat_title( '', false ) . '"' . $after;
		} elseif ( is_search() ) {
			// search
			echo $before . __( 'Search results for', 'tempera' ) . ' "' . get_search_query() . '"' . $after;
		} elseif ( is_day() ) {
			// daily archive
			echo '<a href="' . get_year_link( get_the_time( 'Y' ) ) . '">' . get_the_time( 'Y' ) . '</a> ' . $separator . ' ';
			echo '<a href="' . get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ) . '">' . get_the_time( 'F' ) . '</a> ' . $separator . ' ';
			echo $before . get_the_time( 'd' ) . $after;
		} elseif ( is_month() ) {
			// monthly archive
			echo '<a href="' . get_year_link( get_the_time( 'Y' ) ) . '">' . get_the_time( 'Y' ) . '</a> ' . $separator . ' ';
			echo $before . get_the_time( 'F' ) . $after;
		} elseif ( is_year() ) {
			// yearly archive
			echo $before . get_the_time( 'Y' ) . $after;
		} elseif ( is_single() && ! is_attachment() ) {
			// single post
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object( get_post_type() );
				$slug      = $post_type->rewrite;
				echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
				if ( $showCurrent == 1 ) {
					echo ' ' . $separator . ' ' . $before . get_the_title() . $after;
				}
			} else {
				$cat = get_the_category();
				if ( ! empty( $cat[0] ) ) {
					$cat = $cat[0];
				} else {
					$cat = false;
				};
				if ( $cat ) {
					$cats = get_category_parents( $cat, true, ' ' . $separator . ' ' );
				} else {
					$cats = false;
				};
				if ( $showCurrent == 0 && $cats ) {
					$cats = preg_replace( "#^(.+)\s$separator\s$#", '$1', $cats );
				}
				echo $cats;
				if ( $showCurrent == 1 ) {
					echo $before . get_the_title() . $after;
				}
			}
		} elseif ( ! is_single() && ! is_page() && get_post_type() != 'post' && ! is_404() ) {
			// some other item
			$post_type = get_post_type_object( get_post_type() );
			echo $before . $post_type->labels->singular_name . $after;
		} elseif ( is_attachment() ) {
			// attachment
			$parent = get_post( $post->post_parent );
			$cat    = get_the_category( $parent->ID );
			if ( ! empty( $cat[0] ) ) {
				$cat = $cat[0];
			} else {
				$cat = false; }
			if ( $cat ) {
				echo get_category_parents( $cat, true, ' ' . $separator . ' ' );
			}
			echo '<a href="' . esc_url( get_permalink( $parent ) ) . '">' . $parent->post_title . '</a>';
			if ( $showCurrent == 1 ) {
				echo ' ' . $separator . ' ' . $before . get_the_title() . $after;
			}
		} elseif ( is_page() && ! $post->post_parent ) {
			// parent page
			if ( $showCurrent == 1 ) {
				echo $before . get_the_title() . $after;
			}
		} elseif ( is_page() && $post->post_parent ) {
			// child page
			$parent_id   = $post->post_parent;
			$breadcrumbs = array();
			while ( $parent_id ) {
				$page          = get_post( $parent_id );
				$breadcrumbs[] = '<a href="' . esc_url( get_permalink( $page->ID ) ) . '">' . get_the_title( $page->ID ) . '</a>';
				$parent_id     = $page->post_parent;
			}
			$breadcrumbs = array_reverse( $breadcrumbs );
			for ( $i = 0; $i < count( $breadcrumbs ); $i++ ) {
				echo $breadcrumbs[ $i ];
				if ( $i != count( $breadcrumbs ) - 1 ) {
					echo ' ' . $separator . ' ';
				}
			}
			if ( $showCurrent == 1 ) {
				echo ' ' . $separator . ' ' . $before . get_the_title() . $after;
			}
		} elseif ( is_tag() ) {
			// tag archive
			echo $before . __( 'Posts tagged', 'tempera' ) . ' "' . single_tag_title( '', false ) . '"' . $after;
		} elseif ( is_author() ) {
			// author archive
			global $author;
			$userdata = get_userdata( $author );
			echo $before . __( 'Articles posted by', 'tempera' ) . ' ' . $userdata->display_name . $after;
		} elseif ( is_404() ) {
			// 404 archive
			echo $before . __( 'Error 404', 'tempera' ) . $after;
		} elseif ( get_post_format() ) {
			// post format
			echo $before . '"' . ucwords( get_post_format() ) . '" ' . __( 'Post format', 'tempera' ) . $after;
		}

		if ( get_query_var( 'paged' ) ) {
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) {
				echo ' (';
			}
			echo __( 'Page', 'tempera' ) . ' ' . get_query_var( 'paged' );
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) {
				echo ')';
			}
		}

		echo '</div>';

	}

} // breadcrumbs


/**
 * Disable Emoji Mess
 * thanks to taniarascia
 */
function disable_wp_emojicons() {
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
	add_filter( 'emoji_svg_url', '__return_false' );
}
add_action( 'init', 'disable_wp_emojicons' );
function disable_emojicons_tinymce( $plugins ) {
	return is_array( $plugins ) ? array_diff( $plugins, array( 'wpemoji' ) ) : array();
}

// remove image sizes parent theme makes
function remove_parent_image_sizes() {
	remove_image_size( 'slider' );
	remove_image_size( 'columns' );
	remove_image_size( 'header' );
}
add_action( 'init', 'remove_parent_image_sizes' );

// eventually move into its own file with the other things that are hooked with init?
function child_theme_setup() {
	add_theme_support( 'editor-styles' );
	add_editor_style( 'stylesheets/main.css' );
	add_theme_support( 'html5', array( 'script', 'style' ) );
	remove_filter( 'get_the_excerpt', 'tempera_excerpt_morelink', 20 );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'rsd_link' );
	add_theme_support( 'wp-block-styles' );
	add_theme_support( 'post-thumbnails' );
	remove_theme_support( 'widgets-block-editor' );
}
add_action( 'after_setup_theme', 'child_theme_setup' );


// for all excerpts, append a continue reading link
function return_improved_continue_reading_link( $some_output ) {
	if ( ! is_attachment() ) {
		$some_output .= '';
	}
	return $some_output;
}
add_filter( 'get_the_excerpt', 'return_improved_continue_reading_link' );


// registers the theme_location of the menu
function register_custom_cpl_menus() {
	register_nav_menus(
		array(
			'reimagining_primary_menu_location'    => __( 'Reimagining Primary Menu' )
		)
	);
}
add_action( 'init', 'register_custom_cpl_menus' );

add_action( 'wp_print_scripts', 'dequeue_parent_theme_scripts' );
function dequeue_parent_theme_scripts() {
	wp_dequeue_script( 'tempera-frontend' );
	wp_dequeue_script( 'tempera-nivoslider' );
}

// do not generate any images when non-image MIMEs (PDFs) are uploaded into media
function disable_image_generation_for_non_mimes() {
	$fallback_sizes = array();
	return $fallback_sizes;
}
add_filter( 'fallback_intermediate_image_sizes', 'disable_image_generation_for_non_mimes' );

// disable autoloading the UAG templates automatically in editor
add_filter( 'ast_block_templates_disable', '__return_true' );

function disable_floc( array $headers ) : array {
	$permissions = [];
	if ( ! empty( $headers['Permissions-Policy'] ) ) {
		// Abort if cohorts has already been added.
		if ( strpos( $headers['Permissions-Policy'], 'interest-cohort' ) !== false ) {
			return $headers;
		}
		$permissions = explode( ',', $headers['Permissions-Policy'] );
	}

	$permissions[] = 'interest-cohort=()';
	$headers['Permissions-Policy'] = implode( ',', $permissions );
	return $headers;
}
add_filter( 'wp_headers', 'disable_floc' );


add_filter( 'relevanssi_valid_status', 'relevanssi_filter_out_private_content' );
function relevanssi_filter_out_private_content( $valid_statii ) {
	return array ( 'publish' );
}

?>
