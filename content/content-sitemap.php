<?php
/**
 * The default template for displaying content
 *
 * @package Cryout Creations
 * @subpackage Tempera
 * @since Tempera 1.0
 */

$options = tempera_get_theme_options();
foreach ($options as $key => $value) {
     ${"$key"} = $value ;
}

?><?php cryout_before_article_hook(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( (( is_sticky() && is_page_template() )?'sticky':'') ); ?>>
		<div class="article-inner">
			<header class="entry-header">
				<h2 class="entry-title">
					<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr ( __( 'Permalink to %s', 'tempera' ) ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
				</h2>
				<?php cryout_post_title_hook(); ?>
				<div class="entry-meta">
					<?php	cryout_post_meta_hook();  ?>
				</div><!-- .entry-meta -->
			</header><!-- .entry-header -->

				<div class="entry-content">
				<?php
					wp_list_pages(array( 
						'title_li' => 'Pages', 
						'sort_column' => 'post_title', 
						'post_status' => 'publish', 
						'post_type' => 'page')
					);
				?>
				</div> <!-- .entry-content -->

			<footer class="entry-meta">
				<?php cryout_post_after_content_hook();  ?>
			</footer>
		</div>
	</article><!-- #post-<?php the_ID(); ?> -->


<?php cryout_after_article_hook(); ?>