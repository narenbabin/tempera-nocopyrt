<?php
/**
 *
 * Note: the cpl_agenda excerpt is filtered through the custom-post-agendas plugin
 * filename is unclear but is required to match the post_type's name
 * e.g. content-name_of_post_type
 * @package Cryout Creations
 * @subpackage Tempera
 * @since Tempera 1.0
 */

$options = tempera_get_theme_options();
foreach ( $options as $key => $value ) {
	 ${"$key"} = $value;
}

?><?php cryout_before_article_hook(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( 'post' ); ?>>
		<div class="article-inner">
			<header class="entry-header">
				<h2 class="entry-title">
					<a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a>
				</h2>

				<?php cryout_post_title_hook(); ?>
				<div class="entry-meta">
					<?php	cryout_post_meta_hook(); ?>
				</div><!-- .entry-meta -->
			</header><!-- .entry-header -->


				<?php if ( is_search() ) : // Display excerpts for search pages ?>

							<?php if ( $tempera_excerptarchive != 'Full Post' ) { ?>
							<div class="entry-summary">
								<?php

								// in inc/search.php
								echo extract_relevant_terms( get_search_query(), wp_strip_all_tags( get_the_content() ), 100, 49, '...' );

								?>
							</div><!-- .entry-summary -->




							<?php } else { ?>
							<div class="entry-content">
								<?php the_content(); ?>
								<?php
								wp_link_pages(
									array(
										'before' => '<div class="page-link"><span>' . __( 'Pages:', 'tempera' ) . '</span>',
										'after'  => '</div>',
									)
								);
								?>
							</div><!-- .entry-content -->
							<?php } ?>

					<?php
			else :
				if ( is_sticky() && $tempera_excerptsticky == 'Full Post' ) {
					$sticky_test = 1;
				} else {
					$sticky_test = 0;
				}
				if ( $tempera_excerpthome != 'Full Post' && $sticky_test == 0 ) {
					?>


							<div class="entry-summary">
							<?php
							the_excerpt();
							?>
							</div><!-- .entry-summary -->
							<?php } else { ?>
							<div class="entry-content">
							<?php the_content(); ?>
							<?php
							wp_link_pages(
								array(
									'before' => '<div class="page-link"><span>' . __( 'Pages:', 'tempera' ) . '</span>',
									'after'  => '</div>',
								)
							);
							?>
							</div><!-- .entry-content -->
							<?php
							}

				endif;
			?>

			<footer class="entry-meta">
				<?php cryout_post_after_content_hook(); ?>
			</footer>
		</div>
	</article><!-- #post-<?php the_ID(); ?> -->


<?php cryout_after_article_hook(); ?>
