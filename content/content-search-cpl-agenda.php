<?php
/**
 * An outer template for displaying Search results for CPL agendas; does not display the search form on the results page
 *
 * @package Cryout Creations
 * @subpackage Tempera
 * @since Tempera 1.0
 */

?>

			<?php if ( have_posts() ) : ?>

				<h1 class="entry-title"><?php printf( __( 'Search Results from past CPL Board Agendas for: %s', 'tempera' ), '<i>' . get_search_query() . '</i>' ); ?></h1>


				<?php

				get_template_part( 'templates/partials/form', 'search-cpl-agenda' );
				?>
				<p>The most recent agendas are returned first.</p>
				<?php /* Start the Loop */ ?>
				<?php
				while ( have_posts() ) :
					the_post();
					?>

									<?php
									/* Run the loop for the search to output the results.
									* If you want to overload this in a child theme then include a file
									* called loop-search.php and that will be used instead.
									*/
									get_template_part( 'content/content', get_post_type() );
									?>
										<?php endwhile; ?>

				<?php
					tempera_pagination();
				?>

			<?php else : ?>

				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php printf( __( 'No search results for: %s.', 'tempera' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
					</header><!-- .entry-header -->

					<p>Perhaps check the entire CPL site </p>
					<?php get_search_form(); ?>
				</article><!-- #post-0 -->

				<?php endif; ?>


			<?php cryout_after_content_hook(); ?>
			</div><!-- #content -->
		<?php tempera_get_sidebar(); ?>
		</section><!-- #primary -->

<?php get_footer(); ?>
