<?php
/** 
 * A partial template that displays the 2 most recent titles
 * of custom post type cpl_agenda 
 * ONLY used on the board page
 * @package Cryout Creations
 * @subpackage Tempera
 * @since Tempera 1.0
 */

$options = tempera_get_theme_options();
foreach ( $options as $key => $value ) {
	 ${"$key"} = $value;
}

?><?php //cryout_before_article_hook(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( ( ( is_sticky() && is_page_template() ) ? 'sticky' : '' ) ); ?>>
		<div class="article-inner">
			<header class="entry-header">
				<h3>
					<a href="<?php the_permalink(); ?>"  rel="bookmark"> Board of Trustees <?php the_field( 'cpl_agenda_board_type_field' ); ?> Meeting <?php the_field( 'cpl_agenda_date_picker' ); ?> Agenda </a>
				</h3>

		</div>
	</article><!-- #post-<?php the_ID(); ?> -->
<?php cryout_after_article_hook(); ?>
