<?php
/**
 * Search form
 */

// create unique integer to create labels for searchform, when there's multiple search forms on pg.
$search_unique_id = esc_attr( wp_unique_id() );
?>

<form role="search" method="get" class="form-search"
action="<?php echo esc_url( home_url( '/' ) ); ?>">
<label for="search-input-<?php echo ( $search_unique_id ); ?>" class="label-large">Enter what you want to search for:</label>
<input type="search" id="search-input-<?php echo ( $search_unique_id ); ?>" required class="search-field" placeholder="<?php echo '" name="s"'; ?> />

  <fieldset>
	<legend>Select a source for your search:</legend>
	<div class="radio-row">
	  <label for="catalog"><input type="radio" name="search-form-source" id="catalog" value="catalog"/>
	  Catalog</label>
	  <label for="overdrive"><input type="radio" name="search-form-source" id="overdrive" value="overdrive"/>
	  Ebooks</label>
	  <label for="digitalgallery"><input type="radio" name="search-form-source" id="digitalgallery" value="digitalgallery"/>
	  Digital Gallery</label>
	  <label for="cpl-dot-org"><input type="radio" name="search-form-source" id="cpl-dot-org" value="cpl-dot-org"/>
	  Website</label>
	</div>
  </fieldset>
<input class="cpl-button cpl-button-action--search" value="Submit search" name="search" aria-label="Submit your search query" alt="Search" type="submit" />
</form>

