<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package Cryout Creations
 * @subpackage tempera
 * @since tempera 0.5
 */
?>	<div style="clear:both;"></div>
	</div> <!-- #forbottom -->
</div><!-- #main -->

<!-- LibAnswers chat widget -->
<div id="libchat_263d4586f759602fd2c4cd2f0ec2d665"></div>
<!-- End widget -->


	<footer id="footer" role="contentinfo">
		<div id="colophon" class="l-contained">

			<?php get_sidebar( 'footer' ); ?>

		</div><!-- #colophon -->

		<div class="footer-bottom">
			<div class="l-contained">
				<p class="copyright">&copy;<?php echo date( 'Y' ); ?> Cleveland Public Library<br>"The People's University" - All Rights Reserved</p>
<a class="clevnet" href="https://www.ohiocenterforthebook.org/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ocb-logo-footer.png" alt="Ohio Center for the Book at Cleveland Public Library"></a>&nbsp; &nbsp;<a class="clevnet" href="https://www.clevnet.org/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/clevnet.jpg" alt="Proud Member of CLEVNET Library Cooperation"></a>
			</div>
		</div>
	</footer><!-- #footer -->



</div><!-- #wrapper -->


<?php	wp_footer(); ?>
</body>
</html>
