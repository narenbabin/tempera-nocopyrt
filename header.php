<?php
/**
 * the header
 *
 * displays all of the <head> section and everything up till <div id="main">
 *
 * @package cryout creations
 * @subpackage tempera
 * @since tempera 0.5
 */
?><!doctype html>

<html <?php language_attributes(); ?>>
<head>
<!-- LibAnswers chat widget script -->
<script src="https://v2.libanswers.com/load_chat.php?hash=263d4586f759602fd2c4cd2f0ec2d665"></script>
<!-- End widget script -->

<?php cryout_meta_hook(); ?>

<meta name="facebook-domain-verification" content="186lm8k7joyn81bmd2leqjzi5df023" />
<meta http-equiv="content-type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
<link rel="icon" href="/favicon.ico">

<?php
	wp_head();
?>
	<link rel="stylesheet" id="custom" href="<?php echo get_stylesheet_directory_uri() . '/stylesheets/main.css?' . filemtime( get_stylesheet_directory() . '/stylesheets/main.css' ); ?>" type="text/css" media="all" />
</head>
<body <?php body_class(); ?>>

<?php cryout_body_hook(); ?>


<div id="wrapper" class="hfeed">
<?php cryout_wrapper_hook(); ?>

<header role="banner">
	<a class="screen-reader-text skip-link" href="#content">Skip to content</a>
	<div class="l-top">
		<div class="l-contained l-contained--top">
			<div class="l-top-left">
				<a class="block--cpl-branding" href="/">
					<span class="screen-reader-text">homepage of Cleveland Public Library</span>
				</a>
			</div> <!-- l-top-left -->
			<div class="l-top-right">
				<?php cryout_header_widgets_hook(); ?>
			</div> <!-- l-top-right -->
		</div> <!-- l-contained l-contained top -->

		<?php get_template_part( 'content/content', 'announcement' ); ?>
		<?php
		// if the current post/page, parent page or a page ancestor has future in it
		// is which means its for the reimagining/our future is building/FMP initiative
		// load menu based on that ^
		global $post;

		if ( ! empty( $post ) ) {
			$parents          = get_post_ancestors( $post->ID );
			$reimagining_post = get_page_by_path( 'future' );
			// check of $parent_id
			if ( $reimagining_post->ID == $post->ID || in_array( $reimagining_post->ID, $parents ) ) {
				get_template_part( 'templates/partials/header', 'reimagining' );
			} else {
				get_template_part( 'templates/partials/header', 'default' );
			}
		} else {
			get_template_part( 'templates/partials/header', 'default' );
		}
		?>

	</div> <!-- l-top -->
</header>

<!--- this is will what will display inside the modal that is opened -->
<div id="cpl-dialog-search" data-modal>
  <div data-modal-document>
	<!-- <div class="screen-reader-text">and select a search source.</div> -->
	<?php
	get_search_form();
	?>

  </div> <!-- data-modal-document -->
</div> <!-- cpl-dialog-search -->
<div style="clear:both;height:0;"> </div>

<div id="main">
		<?php cryout_main_hook(); ?>
	<div  id="forbottom" >
		<?php cryout_forbottom_hook(); ?>

		<div style="clear:both;"> </div>

		<?php cryout_breadcrumbs_hook(); ?>
