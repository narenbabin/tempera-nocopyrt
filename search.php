<?php
/**
 * The template for displaying Search results pages.
 *
 * @package Cryout Creations
 * @subpackage Tempera
 * @since Tempera 1.0
 */

get_header(); ?>

		<section id="container" itemscope itemtype="https://schema.org/SearchResultsPage" class="<?php echo tempera_get_layout_class(); ?>">
			<div id="content" role="main">
	<?php
	cryout_before_content_hook();

		// load different template based on what post type is being searched for
	if ( isset( $_GET['post_type'] ) && ( $_GET['post_type'] == 'cpl_agenda' ) ) {
		get_template_part( 'content/content', 'search-cpl-agenda' );
	} else {
		get_template_part( 'content/content', 'search-default' );
	}


	cryout_after_content_hook();
	?>
			</div><!-- #content -->
		<?php tempera_get_sidebar(); ?>
		</section><!-- #primary -->

<?php get_footer(); ?>
