The WordPress child theme for cpl.org: 

Pre-requisites: 

* Installation of the [tempera theme](https://www.cryoutcreations.eu/wordpress-themes/tempera) (which acts as the parent theme)

If you would like strictly use this to debug the website, 
you can download (or clone) this repository, unzip it, and place it in the themes folder. 

If you wish to contribute or develop, you'll need to do the following: 

* install grunt globally: `npm install -g grunt`
* install the local package, by running the following within the theme directory: `npm install --save-dev`

Directions: 

1. Edit the scss in the stylesheets folder. 
2. Run `grunt sass` to compile the scss into css. 

Guidelines on code structure are at https://cpl.org/web-standards/


Folder/file locations and structure: 

there's no very tried and true consistent folder naming structure within WordPress themes; of where to place files. Some themes and frameworks (WP's official themes, wprig, roots' sage) have some ideas. 

There are some requirements that we have to follow as a part of the [template hierarchy](https://developer.wordpress.org/themes/basics/template-hierarchy/)

These locations and will likely evolve somewhat quickly as we adapt more into the Gutenberg/block editor ecosystem. 

`content/ `
code that contains the loop (the heart of WordPress) the_post and the_content functions. 
In nearly* all cases, there is a designated/corresponding php file within `templates/` for each file within content. 
notable exception to this though is that single-post_type needs to be in root folder of theme; 

`templates/ `
- originally was used for templates of post_type 'page' (but then later expanded to all post_types)
- contain the larger layout of a page (whether a page should have sidebars, special footers, headers, etc)

https://developer.wordpress.org/themes/template-files-section/page-template-files/

https://developer.wordpress.org/themes/basics/template-files/

https://developer.wordpress.org/themes/basics/linking-theme-files-directories/

`templates/partials `
- partials are page components that can be reused across different post_types; things like headers.  
(I could imagine this, in the future could be for blocks?)

several files in the root folder are required to be there and required to be named as such by WordPress Theme Standards (header.php, search.php, page.php, functions.php, etc)
