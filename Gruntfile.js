module.exports = function(grunt) { 
  const Fiber = require('fibers');
  const sass = require('node-sass');

  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    sass: {
      options: {
        implementation: sass,
        sourceMap: true
      },
      dist: {
        files: {
          'stylesheets/main.css': 'stylesheets/main.scss'
        }
      }
    }
  });

  grunt.registerTask('default', ['sass']);

    //   browserSync: {
    //     default_options: {
    //       bsFiles: {
    //         src: [
    //           "stylesheets/*.css",
    //           "js/*.js",
    //           "*.php"
    //         ]
    //       },
    //       options: {
    //         watchTask: true
    //       }
    //     }
    //   }
    // });

    // grunt.loadNpmTasks('grunt-contrib-watch');
    // grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-sass');

    // // Launch BrowserSync + watch task
    // grunt.registerTask('default', ['browserSync', 'watch']);
};
