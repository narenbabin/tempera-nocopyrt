<?php

add_action( 'init', 'register_cpl_block_pattern_categories' );

function register_cpl_block_pattern_categories() {
	register_block_pattern_category(
		'alert',
		array( 'label' => __( 'Alerts', 'tempera-nocopyrt' ) )
	);
}
register_block_pattern(
	'tempera-nocopyrt/alert-informational',
	array(
		'title'       => __( 'Alert - Informational', 'tempera-nocpyrt' ),
		'description' => _x( '', 'informational alert for the ' ),
		'categories'  => array( 'alert' ),
		'content'     => "<!-- wp:group {\"className\":\"cpl-alert cpl-alert\\u002d\\u002dinfo\"} -->\n<div class=\"wp-block-group cpl-alert cpl-alert--info\"><div class=\"wp-block-group__inner-container\"><!-- wp:group {\"className\":\"cpl-alert__body\"} -->\n<div class=\"wp-block-group cpl-alert__body\"><div class=\"wp-block-group__inner-container\"><!-- wp:heading {\"className\":\"cpl-alert__heading\"} -->\n<h2 class=\"cpl-alert__heading\">Informational Heading</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"className\":\"cpl-alert__text\"} -->\n<p class=\"cpl-alert__text\">Edit a sentence or two here</p>\n<!-- /wp:paragraph --></div></div>\n<!-- /wp:group --></div></div>\n<!-- /wp:group -->\n",
	)
);

register_block_pattern(
	'tempera-nocopyrt/alert-success',
	array(
		'title'       => __( 'Alert - Success', 'tempera-nocpyrt' ),
		'description' => _x( '', 'Success alert for the ' ),
		'categories'  => array( 'alert' ),
		'content'     => "<!-- wp:group {\"className\":\"cpl-alert cpl-alert\\u002d\\u002dsuccess\"} -->\n<div class=\"wp-block-group cpl-alert cpl-alert--success\"><div class=\"wp-block-group__inner-container\"><!-- wp:group {\"className\":\"cpl-alert__body\"} -->\n<div class=\"wp-block-group cpl-alert__body\"><div class=\"wp-block-group__inner-container\"><!-- wp:heading {\"className\":\"cpl-alert__heading\"} -->\n<h2 class=\"cpl-alert__heading\">Success Heading</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"className\":\"cpl-alert__text\"} -->\n<p class=\"cpl-alert__text\">Edit a sentence or two here</p>\n<!-- /wp:paragraph --></div></div>\n<!-- /wp:group --></div></div>\n<!-- /wp:group -->\n",
	)
);
