<?php

/*
* search functionality to display the most relevant snippets within the search results
additional references: plugins/relevanssi/lib/excerpts-highlights.php
Credit to Ben Boyter
ref: https://github.com/boyter/php-excerpt;
BSD License
*/

// class BoyterSearchInstance {


	// find the locations of each of the words
// Nothing exciting here. The array_unique is required
// unless you decide to make the words unique before passing in

function find_word_locations( $words, $full_text ) {
	$locations = array();
	if ( is_array( $words ) || is_object( $words ) ) {
		foreach ( $words as $word ) {
			$word_len = strlen( $word );
			$loc      = stripos( $full_text, $word );
			while ( $loc !== false ) {
				$locations[0] = $loc;
				$loc          = stripos( $full_text, $word, $loc + $word_len );
			}
		}
	} else {
		$word_len = strlen( $words );
		$loc      = stripos( $full_text, $words );
		while ( $loc !== false ) {
			$locations[0] = $loc;
			$loc          = stripos( $full_text, $words, $loc + $word_len );
		}
	}
	$locations = array_unique( $locations );

	// If no words were found, show beginning of the full_text
	if ( empty( $locations ) ) {
		$locations[0] = 0;
	}

	sort( $locations );
	return $locations;
}
	// Work out which is the most relevant portion to display
	// This is done by looping over each match and finding the smallest distance between two found
	// strings. The idea being that the closer the terms are the better match the snippet would be.
	// When checking for matches we only change the location if there is a better match.
	// The only exception is where we have only two matches in which case we just take the
	// first as will be equally distant.
function determine_snippet_location( $locations, $previous_count ) {
	// If we only have 1 match we dont actually do the for loop so set to the first
	$start_pos     = $locations[0];
	$loc_count     = count( $locations );
	$smallest_diff = PHP_INT_MAX;

	// If we only have 2 skip as its probably equally relevant
	if ( count( $locations ) > 2 ) {
		// skip the first as we check 1 behind
		for ( $i = 1; $i < $loc_count; $i++ ) {
			if ( $i == $loc_count - 1 ) { // at the end
				$diff = $locations[ $i ] - $locations[ $i - 1 ];
			} else {
				$diff = $locations[ $i + 1 ] - $locations[ $i ];
			}

			if ( $smallest_diff > $diff ) {
				$smallest_diff = $diff;
				$start_pos     = $locations[ $i ];
			}
		}
	}

	$start_pos = $start_pos > $previous_count ? $start_pos - $previous_count : 0;
	return $start_pos;
}

	// 1/6 ratio on previous_count tends to work pretty well and puts the terms
	// in the middle of the extract

	// Skora's notes:
	// as the parameters:
	// get_search_query() is the search query input by the user; replaces $words
	// full_text - tring that you want to first term - words - to look inside and find
	// rel_length - integer, length of the excerpt that you are returning, in characters
	// previous_count - integer, number of characters (roughly) to include before the first
	// indicator, what you want to display at the beginning and end of the entire excerpt
function extract_relevant_terms( $words, $full_text, $rel_length = 300, $previous_count = 50, $indicator = '...' ) {

	$text_length = strlen( $full_text );
	if ( $text_length <= $rel_length ) {
		return $full_text;
	}

	$locations = find_word_locations( $words, $full_text );
	$start_pos = determine_snippet_location( $locations, $previous_count );

	// if we are going to snip too much...
	if ( $text_length - $start_pos < $rel_length ) {
		$start_pos = $start_pos - ( $text_length - $start_pos ) / 2;
	}

	$rel_text = substr( $full_text, $start_pos, $rel_length );

	// check to ensure we dont snip the last word if thats the match
	if ( $start_pos + $rel_length < $text_length ) {
		$rel_text = substr( $rel_text, 0, strrpos( $rel_text, ' ' ) ) . $indicator; // remove last word
	}

	// If we trimmed from the front add ...
	if ( $start_pos != 0 ) {
		$rel_text = $indicator . substr( $rel_text, strpos( $rel_text, ' ' ) + 1 ); // remove first word
	}

	return $rel_text;
}

//}
