<?php


function display_mapbox_map() {
	ob_start();
	?>
	<div id="cpl-map-locations"></div>
	<?php
	return ob_get_clean();
}
add_shortcode( 'mapbox_shortcode', 'display_mapbox_map' );


function display_form_search_cpl_agenda() {
	ob_start();
	get_template_part( 'templates/partials/form', 'search-cpl-agenda' );
	return ob_get_clean();
}
add_shortcode( 'form_search_cpl_agenda_shortcode', 'display_form_search_cpl_agenda' );
