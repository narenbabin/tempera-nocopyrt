var localSource =
	exhibit2020Object.themeDirectory + '/js/2020-exhibit-source.json';
//var localSource = '../../../.././2020-exhibit-source.json';
//const localSource = '../.././manifest.json';

console.log( exhibit2020Object.themeDirectory );
console.log( 'the wall number is ' + exhibit2020Object.exhibit2020WallNumber );

// create elements for the wall
// entrycontent container, and then a direct container of Contentdm CONTENTa
const entryContainer = document.querySelector( '.entry-content' );
var cdmContainer = document.createElement( 'div' );
cdmContainer.className = 'cpl-gallery_2020-exhibit';
entryContainer.appendChild( cdmContainer );
var htmlList = document.createElement( 'ul' );
cdmContainer.appendChild( htmlList );

var baseURL =
	'https://cplorg.contentdm.oclc.org/digital/collection/p4014coll24/id/';

var iiifURL = 'https://cdm16014.contentdm.oclc.org/digital/iiif/p4014coll10/';

// function to get verbose errors since fetch isn't explicit
var handleResponse = function ( theResponse ) {
	return theResponse.json().then( function ( json ) {
		if ( theResponse.ok ) {
			return json;
		} else {
			return Promise.reject( theResponse );
		}
	} );
};

// FOR FULL images:

// for page "https://cdm16014.contentdm.oclc.org/digital/iiif/p4014coll10/1295/full/full/0/default.jpg"

// for the image size,

// https://sheltered-temple-69261.herokuapp.com/cdm16014.contentdm.oclc.org/digital/bl/dmwebservices/index.php?q=dmGetCompoundObjectInfo/p4014coll10/1294/json
// based on the wallnumber, select images from that array, do not
// choose first or last from array b/c they're superfluous.

fetch( localSource )
	.then( handleResponse )
	.then( function ( myJson ) {
		console.log( myJson.node.nodetitle );
		console.log(
			myJson.node.node[ exhibit2020Object.exhibit2020WallNumber ]
		);
		console.log( myJson.node.node[ 1 ] );

		for (
			var i = 1;
			i <
			myJson.node.node[ exhibit2020Object.exhibit2020WallNumber ].page
				.length -
				1;
			i++
		) {
			console.log(
				'image title' +
					myJson.node.node[ exhibit2020Object.exhibit2020WallNumber ]
						.page[ i ].pagetitle
			);

			var listItem = document.createElement( 'li' );
			htmlList.appendChild( listItem );
			var theLink = document.createElement( 'a' );
			listItem.appendChild( theLink );
			theLink.href =
				iiifURL +
				myJson.node.node[ exhibit2020Object.exhibit2020WallNumber ]
					.page[ i ].pageptr +
				'/full/full/0/default.jpg';
			// console.log('the image url ' + myJson.sequences[0].canvases[i].images[0].resource["@id"]);
			// theLink.textContent = myJson.sequences[0].canvases[i].label;
			var theImage = document.createElement( 'img' );
			// customize with 2nd fetch; that will obtain it.
			theImage.setAttribute( 'alt', '' );
			theLink.appendChild( theImage );
			theImage.src =
				iiifURL +
				myJson.node.node[ exhibit2020Object.exhibit2020WallNumber ]
					.page[ i ].pageptr +
				'/full/310,/0/default.jpg';
			const theLinkContent = document.createTextNode(
				myJson.node.node[ exhibit2020Object.exhibit2020WallNumber ]
					.page[ i ].pagetitle
			);
			theLink.appendChild( theLinkContent );
		}
	} );

// create span with SVG arrows as the links
const gallerySpan = document.createElement( 'span' );
entryContainer.appendChild( gallerySpan );
gallerySpan.className = 'cpl-span--2020-exhibit';
const previousWallLinkElement = document.createElement( 'a' );
const nextWallLinkElement = document.createElement( 'a' );
nextWallLinkElement.setAttribute( 'aria-label', 'Visit next gallery wall' );
previousWallLinkElement.setAttribute(
	'aria-label',
	'Visit previous gallery wall'
);
const previousWall = exhibit2020Object.exhibit2020WallNumber - 1;
const nextWall = parseInt( exhibit2020Object.exhibit2020WallNumber ) + 1;
console.log( 'next wall is' + nextWall );
previousWallLinkElement.href =
	'/eventsclasses/exhibits/cleveland-20-20/wall-' + previousWall;
nextWallLinkElement.href =
	'/eventsclasses/exhibits/cleveland-20-20/wall-' + nextWall;

if ( exhibit2020Object.exhibit2020WallNumber > 1 ) {
	createPreviousSVGAndLink();
}

if ( exhibit2020Object.exhibit2020WallNumber < 20 ) {
	createNextSVGAndLink();
}

function createPreviousSVGAndLink() {
	const previousSvg = document.createElementNS(
		'http://www.w3.org/2000/svg',
		'svg'
	);
	// creating SVG attributes,
	previousSvg.setAttribute( 'viewbox', '0 0 400 400' );
	previousSvg.setAttribute( 'width', '100' );
	previousSvg.setAttribute( 'height', '100' );
	previousSvg.setAttribute( 'role', 'img' );
	const previousGroup = document.createElementNS(
		'http://www.w3.org/2000/svg',
		'g'
	);
	const tehleftArrow = document.createElementNS(
		'http://www.w3.org/2000/svg',
		'path'
	);
	// tehleftArrow.setAttribute('stroke-linejoin', 'bevel');
	tehleftArrow.setAttribute(
		'd',
		'M68.7,2.5c2.6,0,5.1,1,7.1,2.9c3.9,3.9,3.9,10.3,0,14.2L45.5,50l30.4,30.4c3.9,3.9,3.9,10.3,0,14.2  c-3.9,3.9-10.3,3.9-14.2,0L24.2,57.1c-1.9-1.9-2.9-4.4-2.9-7.1s1.1-5.2,2.9-7.1L61.6,5.4C63.6,3.5,66.2,2.5,68.7,2.5z'
	);
	previousGroup.appendChild( tehleftArrow );
	previousSvg.appendChild( previousGroup );
	previousWallLinkElement.appendChild( previousSvg );
	gallerySpan.appendChild( previousWallLinkElement );
}

function createNextSVGAndLink() {
	const nextSvg = document.createElementNS(
		'http://www.w3.org/2000/svg',
		'svg'
	);
	// Create the group element.
	nextSvg.setAttribute( 'viewbox', '0 0 400 400' );
	nextSvg.setAttribute( 'width', '100' );
	nextSvg.setAttribute( 'height', '100' );
	nextSvg.setAttribute( 'role', 'img' );
	const nextGroup = document.createElementNS(
		'http://www.w3.org/2000/svg',
		'g'
	);
	const tehRightArrow = document.createElementNS(
		'http://www.w3.org/2000/svg',
		'path'
	);
	//  // tehleftArrow.setAttribute('stroke-linejoin', 'bevel');
	tehRightArrow.setAttribute(
		'd',
		'M31.3,97.5c-2.6,0-5.1-1-7.1-2.9c-3.9-3.9-3.9-10.3,0-14.2L54.5,50L24.2,19.6c-3.9-3.9-3.9-10.3,0-14.2 c3.9-3.9,10.3-3.9,14.2,0l37.5,37.5c1.9,1.9,2.9,4.4,2.9,7.1s-1.1,5.2-2.9,7.1L38.4,94.6C36.4,96.5,33.8,97.5,31.3,97.5z'
	);
	// // tehleftArrow.setAttribute('stroke', '#000');
	// //tehleftArrow.setAttribute('width', '100');
	// //tehleftArrow.setAttribute('height', '100');
	nextGroup.appendChild( tehRightArrow );
	nextSvg.appendChild( nextGroup );
	// // const previousWallLinkElementContent -
	nextWallLinkElement.appendChild( nextSvg );

	gallerySpan.appendChild( nextWallLinkElement );
}

// if next wall element not > 18 do not create it;

// create a container, loop through each link;
// then based on current page, bold that one?

// const paginationList = document.createElement('ul');
// paginationList.className = "list--pagination";
// const htmlList = document.createElement('ul');
// const newPaginationLink = document.createElement('li');

// if (current page == ) {
//   .className = 'list-item__';
// }

// cdmContainer.appendChild(htmlList);
// // get current page link, set a variable that one to have a different class for it;

// for(var xx = 1; xx < 20; xx++ ){

//   paginationList.appendChild()
// }

// var listItem = document.createElement('li');
// htmlList.appendChild(listItem);
// var paginationLink = document.createElement('a');
// paginationLink.href =
// }

// <li> <a href="eventsclasses/exhibits/cleveland-20-20/wall-4/"> Wall 4 </a> </li>
