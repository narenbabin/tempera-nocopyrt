  moment.updateLocale('en-us', {
    meridiem : function (hour) {
        if (hour < 12) {
            return "a.m.";
        } else {
            return "p.m.";
        }
    }
  });

  // Goal:
  // Fetch JSON data containing public events,
  // based on a specified location (libcalACFData.libcalLocationObject);
  // display each event's information
  // an empty libcal-container is in content/content-location.php partial template

  console.log(libcalACFData.libcalLocationObject);
  console.log(libcalACFData.libcalCategoryObject);
  var libcalRequestURL = "https://cpl-halva.herokuapp.com/springshare/libcal/passthrough?what=/events?cal_id=8758&days=60&campus=" + libcalACFData.libcalLocationObject;
   console.log(libcalRequestURL);
   // fetch the JSON of events from specified url,
   // when successful, run function whose argument is the JSON from libcal
    jQuery.getJSON(libcalRequestURL, function(result) {
      var howManyEventsToDisplay = 100;
      if (howManyEventsToDisplay > result.events.length) {
        howManyEventsToDisplay = result.events.length;
      }
      console.log(result);


        var eventContainer = document.getElementById("libcal-container");

        // for each event that we want to display,
        // display information about it within the eventContainer
        for (i = 0; i < howManyEventsToDisplay; i++) {
          let eventsFun = result.events[i];

          let formattedStartTime = moment(eventsFun.start).format("MMMM Do [at ] h:mm a ");

          eventContainer.insertAdjacentHTML(
            "beforeend",
            "<h3>" + eventsFun.title + "</h3>" + "<p>" + eventsFun.description + "</p>" +
             '<div class="cpl-event-detail">' + formattedStartTime + eventsFun.location.name + "</div><hr>"
          );
        }
      //id title start end url location.name remaining seats=.seats - .seats_taken
    });

