
  // code unique to the particular template

// https://momentjs.com/docs/#/customization/am-pm/
moment.updateLocale('en-us', {
    meridiem : function (hour) {
        if (hour < 12) {
            return "a.m.";
        } else {
            return "p.m.";
        }
    }
});

  var libcalRequestURL = "https://cpl-halva.herokuapp.com/springshare/libcal/passthrough?what=/events?cal_id=8758&days=" + libcalACFData.libcalDateObject + "&campus=" + libcalACFData.libcalLocationObject + "&category=" + libcalACFData.libcalCategoryObject;
   console.log(libcalRequestURL);
  console.log(libcalACFData.libcalLocationObject);
    jQuery.getJSON(
      libcalRequestURL,
    function(result) {
      var howManyToDisplay = 100;
      if (howManyToDisplay > result.events.length) {
        howManyToDisplay = result.events.length;
      }
      console.log("moment", moment(result.events[0].start));

      console.log(moment(result.events[0].start).format("MMMM Do, h a"));
      let displayUpcomingEvents = (function(howManyToDisplay) {
        var eventsUL = document.querySelector(".cpl-flex-container");

        for (i = 0; i < howManyToDisplay; i++) {
          let eventsFun = result.events[i];
          // let startTime = new Date(eventsFun.start);
          let formattedStartTime = moment(eventsFun.start).format("MMMM Do, h:mm a");

          let newTitle = eventsFun.title.replace(/Music at Main:/i, '');
          let newLocation = eventsFun.location.name.replace(/Main 0\d - /i, '');

          eventsUL.insertAdjacentHTML(
            "beforeend", '<div class="cpl-flex-item">' + '<img src="' + eventsFun.featured_image + '"></img><h3>' + newTitle + "</h3>" + '<p><strong class="cpl-event-detail"> ' +
            formattedStartTime + "<br>" + eventsFun.campus.name + "<br>" +
            newLocation + "</strong></p>" + eventsFun.description + "</div>"
           );
          // console.log(result.events[i].title);
        }
      })(howManyToDisplay);

      //id title start end url location.name remaining seats=.seats - .seats_taken
    }
  );

