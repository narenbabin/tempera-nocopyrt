<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Cryout Creations
 * @subpackage tempera
 * @since tempera 0.5
 */

get_header(); ?>

	<div id="container" class="<?php echo tempera_get_layout_class(); ?>">
	
		<div id="content" role="main">

			<div id="post-0" class="post error404 not-found">
				<h1 class="entry-title"><?php _e( 'Not Found', 'tempera' ); ?></h1>

				<div class="entry-content">
					<div class="contentsearch">
					<a href="https://cpl.org/wp-content/uploads/cpl-1869-logo.jpg"><img src="https://cpl.org/wp-content/uploads/cpl-1869-logo-300x174.jpg" alt="" width="300" height="174" class="aligncenter size-medium wp-image-9975" /></a>
					<p><?php _e( 'Apologies, but the page you requested could not be found.', 'tempera' ); ?></p>
					
					<p> If you believe this was an error, we are sorry! Please <a href="https://cpl.org/contact/website-question/"> contact us and share 
					what you're trying to find </p>

					</div>
				</div><!-- .entry-content -->
			</div><!-- #post-0 -->

		</div><!-- #content -->
<?php tempera_get_sidebar(); ?>
	</div><!-- #container -->
<?php get_footer(); ?>
